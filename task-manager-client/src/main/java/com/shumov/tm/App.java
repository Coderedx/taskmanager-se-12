package com.shumov.tm;


import com.shumov.tm.bootstrap.Bootstrap;
import com.shumov.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.reflections.Reflections;

import java.util.Set;

public class App{

        @NotNull
        private static final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("com.shumov.tm").getSubTypesOf(AbstractCommand.class);

        public static void main( String[] args )
        {
            @NotNull Bootstrap bootstrap = new Bootstrap();
            try {
                bootstrap.init(classes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

}
