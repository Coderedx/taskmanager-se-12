
package com.shumov.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.shumov.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.shumov.com/", "Exception");
    private final static QName _AddTaskInProject_QNAME = new QName("http://endpoint.tm.shumov.com/", "addTaskInProject");
    private final static QName _AddTaskInProjectResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "addTaskInProjectResponse");
    private final static QName _DataLoadBin_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadBin");
    private final static QName _DataLoadBinResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadBinResponse");
    private final static QName _DataLoadFasterJSON_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadFasterJSON");
    private final static QName _DataLoadFasterJSONResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadFasterJSONResponse");
    private final static QName _DataLoadFasterXML_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadFasterXML");
    private final static QName _DataLoadFasterXMLResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadFasterXMLResponse");
    private final static QName _DataLoadJaxBJSON_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadJaxBJSON");
    private final static QName _DataLoadJaxBJSONResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadJaxBJSONResponse");
    private final static QName _DataLoadJaxBXML_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadJaxBXML");
    private final static QName _DataLoadJaxBXMLResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataLoadJaxBXMLResponse");
    private final static QName _DataSaveBin_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveBin");
    private final static QName _DataSaveBinResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveBinResponse");
    private final static QName _DataSaveFasterJSON_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveFasterJSON");
    private final static QName _DataSaveFasterJSONResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveFasterJSONResponse");
    private final static QName _DataSaveFasterXML_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveFasterXML");
    private final static QName _DataSaveFasterXMLResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveFasterXMLResponse");
    private final static QName _DataSaveJaxBJSON_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveJaxBJSON");
    private final static QName _DataSaveJaxBJSONResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveJaxBJSONResponse");
    private final static QName _DataSaveJaxBXML_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveJaxBXML");
    private final static QName _DataSaveJaxBXMLResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "dataSaveJaxBXMLResponse");
    private final static QName _GetProjectList_QNAME = new QName("http://endpoint.tm.shumov.com/", "getProjectList");
    private final static QName _GetProjectListResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "getProjectListResponse");
    private final static QName _GetProjectTasksById_QNAME = new QName("http://endpoint.tm.shumov.com/", "getProjectTasksById");
    private final static QName _GetProjectTasksByIdResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "getProjectTasksByIdResponse");
    private final static QName _GetTaskList_QNAME = new QName("http://endpoint.tm.shumov.com/", "getTaskList");
    private final static QName _GetTaskListResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "getTaskListResponse");
    private final static QName _ProjectClearDB_QNAME = new QName("http://endpoint.tm.shumov.com/", "projectClearDB");
    private final static QName _ProjectClearDBResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "projectClearDBResponse");
    private final static QName _ProjectEditName_QNAME = new QName("http://endpoint.tm.shumov.com/", "projectEditName");
    private final static QName _ProjectEditNameResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "projectEditNameResponse");
    private final static QName _RegAdmin_QNAME = new QName("http://endpoint.tm.shumov.com/", "regAdmin");
    private final static QName _RegAdminResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "regAdminResponse");
    private final static QName _RemoveProjectById_QNAME = new QName("http://endpoint.tm.shumov.com/", "removeProjectById");
    private final static QName _RemoveProjectByIdResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "removeProjectByIdResponse");
    private final static QName _RemoveTaskById_QNAME = new QName("http://endpoint.tm.shumov.com/", "removeTaskById");
    private final static QName _RemoveTaskByIdResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "removeTaskByIdResponse");
    private final static QName _ReviewUserAdmin_QNAME = new QName("http://endpoint.tm.shumov.com/", "reviewUserAdmin");
    private final static QName _ReviewUserAdminResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "reviewUserAdminResponse");
    private final static QName _TaskClearDB_QNAME = new QName("http://endpoint.tm.shumov.com/", "taskClearDB");
    private final static QName _TaskClearDBResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "taskClearDBResponse");
    private final static QName _TaskEditName_QNAME = new QName("http://endpoint.tm.shumov.com/", "taskEditName");
    private final static QName _TaskEditNameResponse_QNAME = new QName("http://endpoint.tm.shumov.com/", "taskEditNameResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.shumov.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link AddTaskInProject }
     * 
     */
    public AddTaskInProject createAddTaskInProject() {
        return new AddTaskInProject();
    }

    /**
     * Create an instance of {@link AddTaskInProjectResponse }
     * 
     */
    public AddTaskInProjectResponse createAddTaskInProjectResponse() {
        return new AddTaskInProjectResponse();
    }

    /**
     * Create an instance of {@link DataLoadBin }
     * 
     */
    public DataLoadBin createDataLoadBin() {
        return new DataLoadBin();
    }

    /**
     * Create an instance of {@link DataLoadBinResponse }
     * 
     */
    public DataLoadBinResponse createDataLoadBinResponse() {
        return new DataLoadBinResponse();
    }

    /**
     * Create an instance of {@link DataLoadFasterJSON }
     * 
     */
    public DataLoadFasterJSON createDataLoadFasterJSON() {
        return new DataLoadFasterJSON();
    }

    /**
     * Create an instance of {@link DataLoadFasterJSONResponse }
     * 
     */
    public DataLoadFasterJSONResponse createDataLoadFasterJSONResponse() {
        return new DataLoadFasterJSONResponse();
    }

    /**
     * Create an instance of {@link DataLoadFasterXML }
     * 
     */
    public DataLoadFasterXML createDataLoadFasterXML() {
        return new DataLoadFasterXML();
    }

    /**
     * Create an instance of {@link DataLoadFasterXMLResponse }
     * 
     */
    public DataLoadFasterXMLResponse createDataLoadFasterXMLResponse() {
        return new DataLoadFasterXMLResponse();
    }

    /**
     * Create an instance of {@link DataLoadJaxBJSON }
     * 
     */
    public DataLoadJaxBJSON createDataLoadJaxBJSON() {
        return new DataLoadJaxBJSON();
    }

    /**
     * Create an instance of {@link DataLoadJaxBJSONResponse }
     * 
     */
    public DataLoadJaxBJSONResponse createDataLoadJaxBJSONResponse() {
        return new DataLoadJaxBJSONResponse();
    }

    /**
     * Create an instance of {@link DataLoadJaxBXML }
     * 
     */
    public DataLoadJaxBXML createDataLoadJaxBXML() {
        return new DataLoadJaxBXML();
    }

    /**
     * Create an instance of {@link DataLoadJaxBXMLResponse }
     * 
     */
    public DataLoadJaxBXMLResponse createDataLoadJaxBXMLResponse() {
        return new DataLoadJaxBXMLResponse();
    }

    /**
     * Create an instance of {@link DataSaveBin }
     * 
     */
    public DataSaveBin createDataSaveBin() {
        return new DataSaveBin();
    }

    /**
     * Create an instance of {@link DataSaveBinResponse }
     * 
     */
    public DataSaveBinResponse createDataSaveBinResponse() {
        return new DataSaveBinResponse();
    }

    /**
     * Create an instance of {@link DataSaveFasterJSON }
     * 
     */
    public DataSaveFasterJSON createDataSaveFasterJSON() {
        return new DataSaveFasterJSON();
    }

    /**
     * Create an instance of {@link DataSaveFasterJSONResponse }
     * 
     */
    public DataSaveFasterJSONResponse createDataSaveFasterJSONResponse() {
        return new DataSaveFasterJSONResponse();
    }

    /**
     * Create an instance of {@link DataSaveFasterXML }
     * 
     */
    public DataSaveFasterXML createDataSaveFasterXML() {
        return new DataSaveFasterXML();
    }

    /**
     * Create an instance of {@link DataSaveFasterXMLResponse }
     * 
     */
    public DataSaveFasterXMLResponse createDataSaveFasterXMLResponse() {
        return new DataSaveFasterXMLResponse();
    }

    /**
     * Create an instance of {@link DataSaveJaxBJSON }
     * 
     */
    public DataSaveJaxBJSON createDataSaveJaxBJSON() {
        return new DataSaveJaxBJSON();
    }

    /**
     * Create an instance of {@link DataSaveJaxBJSONResponse }
     * 
     */
    public DataSaveJaxBJSONResponse createDataSaveJaxBJSONResponse() {
        return new DataSaveJaxBJSONResponse();
    }

    /**
     * Create an instance of {@link DataSaveJaxBXML }
     * 
     */
    public DataSaveJaxBXML createDataSaveJaxBXML() {
        return new DataSaveJaxBXML();
    }

    /**
     * Create an instance of {@link DataSaveJaxBXMLResponse }
     * 
     */
    public DataSaveJaxBXMLResponse createDataSaveJaxBXMLResponse() {
        return new DataSaveJaxBXMLResponse();
    }

    /**
     * Create an instance of {@link GetProjectList }
     * 
     */
    public GetProjectList createGetProjectList() {
        return new GetProjectList();
    }

    /**
     * Create an instance of {@link GetProjectListResponse }
     * 
     */
    public GetProjectListResponse createGetProjectListResponse() {
        return new GetProjectListResponse();
    }

    /**
     * Create an instance of {@link GetProjectTasksById }
     * 
     */
    public GetProjectTasksById createGetProjectTasksById() {
        return new GetProjectTasksById();
    }

    /**
     * Create an instance of {@link GetProjectTasksByIdResponse }
     * 
     */
    public GetProjectTasksByIdResponse createGetProjectTasksByIdResponse() {
        return new GetProjectTasksByIdResponse();
    }

    /**
     * Create an instance of {@link GetTaskList }
     * 
     */
    public GetTaskList createGetTaskList() {
        return new GetTaskList();
    }

    /**
     * Create an instance of {@link GetTaskListResponse }
     * 
     */
    public GetTaskListResponse createGetTaskListResponse() {
        return new GetTaskListResponse();
    }

    /**
     * Create an instance of {@link ProjectClearDB }
     * 
     */
    public ProjectClearDB createProjectClearDB() {
        return new ProjectClearDB();
    }

    /**
     * Create an instance of {@link ProjectClearDBResponse }
     * 
     */
    public ProjectClearDBResponse createProjectClearDBResponse() {
        return new ProjectClearDBResponse();
    }

    /**
     * Create an instance of {@link ProjectEditName }
     * 
     */
    public ProjectEditName createProjectEditName() {
        return new ProjectEditName();
    }

    /**
     * Create an instance of {@link ProjectEditNameResponse }
     * 
     */
    public ProjectEditNameResponse createProjectEditNameResponse() {
        return new ProjectEditNameResponse();
    }

    /**
     * Create an instance of {@link RegAdmin }
     * 
     */
    public RegAdmin createRegAdmin() {
        return new RegAdmin();
    }

    /**
     * Create an instance of {@link RegAdminResponse }
     * 
     */
    public RegAdminResponse createRegAdminResponse() {
        return new RegAdminResponse();
    }

    /**
     * Create an instance of {@link RemoveProjectById }
     * 
     */
    public RemoveProjectById createRemoveProjectById() {
        return new RemoveProjectById();
    }

    /**
     * Create an instance of {@link RemoveProjectByIdResponse }
     * 
     */
    public RemoveProjectByIdResponse createRemoveProjectByIdResponse() {
        return new RemoveProjectByIdResponse();
    }

    /**
     * Create an instance of {@link RemoveTaskById }
     * 
     */
    public RemoveTaskById createRemoveTaskById() {
        return new RemoveTaskById();
    }

    /**
     * Create an instance of {@link RemoveTaskByIdResponse }
     * 
     */
    public RemoveTaskByIdResponse createRemoveTaskByIdResponse() {
        return new RemoveTaskByIdResponse();
    }

    /**
     * Create an instance of {@link ReviewUserAdmin }
     * 
     */
    public ReviewUserAdmin createReviewUserAdmin() {
        return new ReviewUserAdmin();
    }

    /**
     * Create an instance of {@link ReviewUserAdminResponse }
     * 
     */
    public ReviewUserAdminResponse createReviewUserAdminResponse() {
        return new ReviewUserAdminResponse();
    }

    /**
     * Create an instance of {@link TaskClearDB }
     * 
     */
    public TaskClearDB createTaskClearDB() {
        return new TaskClearDB();
    }

    /**
     * Create an instance of {@link TaskClearDBResponse }
     * 
     */
    public TaskClearDBResponse createTaskClearDBResponse() {
        return new TaskClearDBResponse();
    }

    /**
     * Create an instance of {@link TaskEditName }
     * 
     */
    public TaskEditName createTaskEditName() {
        return new TaskEditName();
    }

    /**
     * Create an instance of {@link TaskEditNameResponse }
     * 
     */
    public TaskEditNameResponse createTaskEditNameResponse() {
        return new TaskEditNameResponse();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link Task }
     * 
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link AbstractEntity }
     * 
     */
    public AbstractEntity createAbstractEntity() {
        return new AbstractEntity();
    }

    /**
     * Create an instance of {@link Project }
     * 
     */
    public Project createProject() {
        return new Project();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskInProject }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddTaskInProject }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "addTaskInProject")
    public JAXBElement<AddTaskInProject> createAddTaskInProject(AddTaskInProject value) {
        return new JAXBElement<AddTaskInProject>(_AddTaskInProject_QNAME, AddTaskInProject.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddTaskInProjectResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AddTaskInProjectResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "addTaskInProjectResponse")
    public JAXBElement<AddTaskInProjectResponse> createAddTaskInProjectResponse(AddTaskInProjectResponse value) {
        return new JAXBElement<AddTaskInProjectResponse>(_AddTaskInProjectResponse_QNAME, AddTaskInProjectResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadBin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadBin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadBin")
    public JAXBElement<DataLoadBin> createDataLoadBin(DataLoadBin value) {
        return new JAXBElement<DataLoadBin>(_DataLoadBin_QNAME, DataLoadBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadBinResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadBinResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadBinResponse")
    public JAXBElement<DataLoadBinResponse> createDataLoadBinResponse(DataLoadBinResponse value) {
        return new JAXBElement<DataLoadBinResponse>(_DataLoadBinResponse_QNAME, DataLoadBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadFasterJSON }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadFasterJSON }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadFasterJSON")
    public JAXBElement<DataLoadFasterJSON> createDataLoadFasterJSON(DataLoadFasterJSON value) {
        return new JAXBElement<DataLoadFasterJSON>(_DataLoadFasterJSON_QNAME, DataLoadFasterJSON.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadFasterJSONResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadFasterJSONResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadFasterJSONResponse")
    public JAXBElement<DataLoadFasterJSONResponse> createDataLoadFasterJSONResponse(DataLoadFasterJSONResponse value) {
        return new JAXBElement<DataLoadFasterJSONResponse>(_DataLoadFasterJSONResponse_QNAME, DataLoadFasterJSONResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadFasterXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadFasterXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadFasterXML")
    public JAXBElement<DataLoadFasterXML> createDataLoadFasterXML(DataLoadFasterXML value) {
        return new JAXBElement<DataLoadFasterXML>(_DataLoadFasterXML_QNAME, DataLoadFasterXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadFasterXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadFasterXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadFasterXMLResponse")
    public JAXBElement<DataLoadFasterXMLResponse> createDataLoadFasterXMLResponse(DataLoadFasterXMLResponse value) {
        return new JAXBElement<DataLoadFasterXMLResponse>(_DataLoadFasterXMLResponse_QNAME, DataLoadFasterXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadJaxBJSON }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadJaxBJSON }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadJaxBJSON")
    public JAXBElement<DataLoadJaxBJSON> createDataLoadJaxBJSON(DataLoadJaxBJSON value) {
        return new JAXBElement<DataLoadJaxBJSON>(_DataLoadJaxBJSON_QNAME, DataLoadJaxBJSON.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadJaxBJSONResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadJaxBJSONResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadJaxBJSONResponse")
    public JAXBElement<DataLoadJaxBJSONResponse> createDataLoadJaxBJSONResponse(DataLoadJaxBJSONResponse value) {
        return new JAXBElement<DataLoadJaxBJSONResponse>(_DataLoadJaxBJSONResponse_QNAME, DataLoadJaxBJSONResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadJaxBXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadJaxBXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadJaxBXML")
    public JAXBElement<DataLoadJaxBXML> createDataLoadJaxBXML(DataLoadJaxBXML value) {
        return new JAXBElement<DataLoadJaxBXML>(_DataLoadJaxBXML_QNAME, DataLoadJaxBXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoadJaxBXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataLoadJaxBXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataLoadJaxBXMLResponse")
    public JAXBElement<DataLoadJaxBXMLResponse> createDataLoadJaxBXMLResponse(DataLoadJaxBXMLResponse value) {
        return new JAXBElement<DataLoadJaxBXMLResponse>(_DataLoadJaxBXMLResponse_QNAME, DataLoadJaxBXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveBin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveBin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveBin")
    public JAXBElement<DataSaveBin> createDataSaveBin(DataSaveBin value) {
        return new JAXBElement<DataSaveBin>(_DataSaveBin_QNAME, DataSaveBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveBinResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveBinResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveBinResponse")
    public JAXBElement<DataSaveBinResponse> createDataSaveBinResponse(DataSaveBinResponse value) {
        return new JAXBElement<DataSaveBinResponse>(_DataSaveBinResponse_QNAME, DataSaveBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveFasterJSON }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveFasterJSON }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveFasterJSON")
    public JAXBElement<DataSaveFasterJSON> createDataSaveFasterJSON(DataSaveFasterJSON value) {
        return new JAXBElement<DataSaveFasterJSON>(_DataSaveFasterJSON_QNAME, DataSaveFasterJSON.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveFasterJSONResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveFasterJSONResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveFasterJSONResponse")
    public JAXBElement<DataSaveFasterJSONResponse> createDataSaveFasterJSONResponse(DataSaveFasterJSONResponse value) {
        return new JAXBElement<DataSaveFasterJSONResponse>(_DataSaveFasterJSONResponse_QNAME, DataSaveFasterJSONResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveFasterXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveFasterXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveFasterXML")
    public JAXBElement<DataSaveFasterXML> createDataSaveFasterXML(DataSaveFasterXML value) {
        return new JAXBElement<DataSaveFasterXML>(_DataSaveFasterXML_QNAME, DataSaveFasterXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveFasterXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveFasterXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveFasterXMLResponse")
    public JAXBElement<DataSaveFasterXMLResponse> createDataSaveFasterXMLResponse(DataSaveFasterXMLResponse value) {
        return new JAXBElement<DataSaveFasterXMLResponse>(_DataSaveFasterXMLResponse_QNAME, DataSaveFasterXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveJaxBJSON }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveJaxBJSON }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveJaxBJSON")
    public JAXBElement<DataSaveJaxBJSON> createDataSaveJaxBJSON(DataSaveJaxBJSON value) {
        return new JAXBElement<DataSaveJaxBJSON>(_DataSaveJaxBJSON_QNAME, DataSaveJaxBJSON.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveJaxBJSONResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveJaxBJSONResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveJaxBJSONResponse")
    public JAXBElement<DataSaveJaxBJSONResponse> createDataSaveJaxBJSONResponse(DataSaveJaxBJSONResponse value) {
        return new JAXBElement<DataSaveJaxBJSONResponse>(_DataSaveJaxBJSONResponse_QNAME, DataSaveJaxBJSONResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveJaxBXML }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveJaxBXML }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveJaxBXML")
    public JAXBElement<DataSaveJaxBXML> createDataSaveJaxBXML(DataSaveJaxBXML value) {
        return new JAXBElement<DataSaveJaxBXML>(_DataSaveJaxBXML_QNAME, DataSaveJaxBXML.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataSaveJaxBXMLResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataSaveJaxBXMLResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "dataSaveJaxBXMLResponse")
    public JAXBElement<DataSaveJaxBXMLResponse> createDataSaveJaxBXMLResponse(DataSaveJaxBXMLResponse value) {
        return new JAXBElement<DataSaveJaxBXMLResponse>(_DataSaveJaxBXMLResponse_QNAME, DataSaveJaxBXMLResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProjectList }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "getProjectList")
    public JAXBElement<GetProjectList> createGetProjectList(GetProjectList value) {
        return new JAXBElement<GetProjectList>(_GetProjectList_QNAME, GetProjectList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProjectListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "getProjectListResponse")
    public JAXBElement<GetProjectListResponse> createGetProjectListResponse(GetProjectListResponse value) {
        return new JAXBElement<GetProjectListResponse>(_GetProjectListResponse_QNAME, GetProjectListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectTasksById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProjectTasksById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "getProjectTasksById")
    public JAXBElement<GetProjectTasksById> createGetProjectTasksById(GetProjectTasksById value) {
        return new JAXBElement<GetProjectTasksById>(_GetProjectTasksById_QNAME, GetProjectTasksById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProjectTasksByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetProjectTasksByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "getProjectTasksByIdResponse")
    public JAXBElement<GetProjectTasksByIdResponse> createGetProjectTasksByIdResponse(GetProjectTasksByIdResponse value) {
        return new JAXBElement<GetProjectTasksByIdResponse>(_GetProjectTasksByIdResponse_QNAME, GetProjectTasksByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTaskList }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTaskList }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "getTaskList")
    public JAXBElement<GetTaskList> createGetTaskList(GetTaskList value) {
        return new JAXBElement<GetTaskList>(_GetTaskList_QNAME, GetTaskList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTaskListResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GetTaskListResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "getTaskListResponse")
    public JAXBElement<GetTaskListResponse> createGetTaskListResponse(GetTaskListResponse value) {
        return new JAXBElement<GetTaskListResponse>(_GetTaskListResponse_QNAME, GetTaskListResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProjectClearDB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ProjectClearDB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "projectClearDB")
    public JAXBElement<ProjectClearDB> createProjectClearDB(ProjectClearDB value) {
        return new JAXBElement<ProjectClearDB>(_ProjectClearDB_QNAME, ProjectClearDB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProjectClearDBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ProjectClearDBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "projectClearDBResponse")
    public JAXBElement<ProjectClearDBResponse> createProjectClearDBResponse(ProjectClearDBResponse value) {
        return new JAXBElement<ProjectClearDBResponse>(_ProjectClearDBResponse_QNAME, ProjectClearDBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProjectEditName }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ProjectEditName }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "projectEditName")
    public JAXBElement<ProjectEditName> createProjectEditName(ProjectEditName value) {
        return new JAXBElement<ProjectEditName>(_ProjectEditName_QNAME, ProjectEditName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProjectEditNameResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ProjectEditNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "projectEditNameResponse")
    public JAXBElement<ProjectEditNameResponse> createProjectEditNameResponse(ProjectEditNameResponse value) {
        return new JAXBElement<ProjectEditNameResponse>(_ProjectEditNameResponse_QNAME, ProjectEditNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegAdmin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RegAdmin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "regAdmin")
    public JAXBElement<RegAdmin> createRegAdmin(RegAdmin value) {
        return new JAXBElement<RegAdmin>(_RegAdmin_QNAME, RegAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegAdminResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RegAdminResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "regAdminResponse")
    public JAXBElement<RegAdminResponse> createRegAdminResponse(RegAdminResponse value) {
        return new JAXBElement<RegAdminResponse>(_RegAdminResponse_QNAME, RegAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveProjectById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "removeProjectById")
    public JAXBElement<RemoveProjectById> createRemoveProjectById(RemoveProjectById value) {
        return new JAXBElement<RemoveProjectById>(_RemoveProjectById_QNAME, RemoveProjectById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveProjectByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "removeProjectByIdResponse")
    public JAXBElement<RemoveProjectByIdResponse> createRemoveProjectByIdResponse(RemoveProjectByIdResponse value) {
        return new JAXBElement<RemoveProjectByIdResponse>(_RemoveProjectByIdResponse_QNAME, RemoveProjectByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskById }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskById }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "removeTaskById")
    public JAXBElement<RemoveTaskById> createRemoveTaskById(RemoveTaskById value) {
        return new JAXBElement<RemoveTaskById>(_RemoveTaskById_QNAME, RemoveTaskById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveTaskByIdResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "removeTaskByIdResponse")
    public JAXBElement<RemoveTaskByIdResponse> createRemoveTaskByIdResponse(RemoveTaskByIdResponse value) {
        return new JAXBElement<RemoveTaskByIdResponse>(_RemoveTaskByIdResponse_QNAME, RemoveTaskByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReviewUserAdmin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ReviewUserAdmin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "reviewUserAdmin")
    public JAXBElement<ReviewUserAdmin> createReviewUserAdmin(ReviewUserAdmin value) {
        return new JAXBElement<ReviewUserAdmin>(_ReviewUserAdmin_QNAME, ReviewUserAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReviewUserAdminResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ReviewUserAdminResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "reviewUserAdminResponse")
    public JAXBElement<ReviewUserAdminResponse> createReviewUserAdminResponse(ReviewUserAdminResponse value) {
        return new JAXBElement<ReviewUserAdminResponse>(_ReviewUserAdminResponse_QNAME, ReviewUserAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaskClearDB }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link TaskClearDB }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "taskClearDB")
    public JAXBElement<TaskClearDB> createTaskClearDB(TaskClearDB value) {
        return new JAXBElement<TaskClearDB>(_TaskClearDB_QNAME, TaskClearDB.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaskClearDBResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link TaskClearDBResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "taskClearDBResponse")
    public JAXBElement<TaskClearDBResponse> createTaskClearDBResponse(TaskClearDBResponse value) {
        return new JAXBElement<TaskClearDBResponse>(_TaskClearDBResponse_QNAME, TaskClearDBResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaskEditName }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link TaskEditName }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "taskEditName")
    public JAXBElement<TaskEditName> createTaskEditName(TaskEditName value) {
        return new JAXBElement<TaskEditName>(_TaskEditName_QNAME, TaskEditName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaskEditNameResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link TaskEditNameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.shumov.com/", name = "taskEditNameResponse")
    public JAXBElement<TaskEditNameResponse> createTaskEditNameResponse(TaskEditNameResponse value) {
        return new JAXBElement<TaskEditNameResponse>(_TaskEditNameResponse_QNAME, TaskEditNameResponse.class, null, value);
    }

}
