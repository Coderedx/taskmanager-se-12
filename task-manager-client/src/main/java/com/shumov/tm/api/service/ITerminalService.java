package com.shumov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Date;

public interface ITerminalService {
    @NotNull
    String nextLine();

    void isCorrectInputData(@Nullable final String name) throws Exception;

    @NotNull Date parseDate (@Nullable final String date) throws IOException;

}
