package com.shumov.tm.api.service;

import com.shumov.tm.command.AbstractCommand;

import java.util.Map;

public interface ServiceLocator {

    ITerminalService getTerminalService();

    com.shumov.tm.endpoint.AdminEndpointService getAdminEndpoint();

    com.shumov.tm.endpoint.ProjectEndpointService getProjectEndpoint();

    com.shumov.tm.endpoint.TaskEndpointService getTaskEndpoint();

    com.shumov.tm.endpoint.SessionEndpointService getSessionEndpoint();

    com.shumov.tm.endpoint.UserDTO getCurrentUser();

    com.shumov.tm.endpoint.Session getSession();

    void setCurrentUser(com.shumov.tm.endpoint.UserDTO currentUser);

    void setSession(com.shumov.tm.endpoint.Session session);

    Map<String, AbstractCommand> getCommands();
}
