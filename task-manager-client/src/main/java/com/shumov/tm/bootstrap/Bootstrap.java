package com.shumov.tm.bootstrap;


import com.shumov.tm.api.service.*;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import com.shumov.tm.exception.command.CommandCorruptException;
import com.shumov.tm.exception.command.CommandWrongException;
import com.shumov.tm.service.TerminalService;
import lombok.Getter;
import lombok.NoArgsConstructor;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.io.IOException;
import java.lang.Exception;
import java.util.*;

@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull
    @Getter
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    @NotNull
    @Getter
    private ITerminalService terminalService = new TerminalService();
    @NotNull
    @Getter
    private AdminEndpointService adminEndpoint = new AdminEndpointService();
    @NotNull
    @Getter
    private ProjectEndpointService projectEndpoint = new ProjectEndpointService();
    @NotNull
    @Getter
    private TaskEndpointService taskEndpoint = new TaskEndpointService();
    @NotNull
    @Getter
    private SessionEndpointService sessionEndpoint = new SessionEndpointService();
    @Nullable
    @Getter
    @Setter
    private UserDTO currentUser;
    @Nullable
    @Getter
    @Setter
    private Session session;

    public void init(@NotNull final Set classes) throws Exception {
        commands.clear();
        initUser();
        initCommands(classes);
        start();
    }

    private void registry(@NotNull final AbstractCommand command) throws CommandCorruptException {
        @NotNull final String cliCommand = command.command();
        @NotNull final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty()) {
            throw new CommandCorruptException();
        }
        if (cliDescription == null || cliDescription.isEmpty()) {
            throw new CommandCorruptException();
        }
        command.setServiceLocator(this);
        commands.put(command.command(), command);
    }

    private void start() {
        System.out.println("[WELCOME TO TASK MANAGER]");
        System.out.println("[ENTER \"help\" TO GET COMMAND LIST]");
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            System.out.println("\nENTER COMMAND:");
            command = terminalService.nextLine();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@NotNull final String command) throws Exception {
        if (command == null || command.isEmpty()) {
            throw new CommandWrongException();
        }
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            throw new CommandWrongException();
        }
        // Проверка валидности выполнения операции.
        if(isSecureCommand(abstractCommand)){
            abstractCommand.execute();
        } else {
            throw new IOException("access error!".toUpperCase());
        }
    }

    private void initCommands(@NotNull final Set classes) throws Exception {
        for (@NotNull final Object clazz : classes){
            if(AbstractCommand.class.isAssignableFrom((Class) clazz)) {
                @NotNull final AbstractCommand command =(AbstractCommand) ((Class) clazz).newInstance();
                command.initRoles();
                registry(command);
            }
        }
    }

    private void initUser() throws Exception {
        currentUser = new UserDTO();
        currentUser.setRole(Role.GUEST);
    }

    private boolean isSecureCommand(@NotNull final AbstractCommand command) {
        @Nullable Role role = currentUser.getRole();
        @NotNull final List<Role> roles = command.getRoleTypes();
        return roles.contains(role);
    }
}
