package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class UserUpdPassCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-pass";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Set new password for current user";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[SET NEW PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String password = terminalService.nextLine();
        terminalService.isCorrectInputData(password);
        @NotNull final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        userEndpoint.editPassword(session,password);
        System.out.println("Password changed successfully".toUpperCase());
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
        roleTypes.add(Role.USER);
    }
}
