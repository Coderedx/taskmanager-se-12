package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.List;

public class TaskListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-list";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all tasks";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK LIST]");
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Task> list = taskEndpoint.getTaskList(session);
        for (@NotNull final Task task : list) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName()
                    + " PROJECT ID: "+ task.getIdProject());
        }
    }
}
