package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskCreateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new task";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK CREATE]\nENTER TASK NAME:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String taskName = terminalService.nextLine();
        terminalService.isCorrectInputData(taskName);
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = terminalService.nextLine();
        terminalService.isCorrectInputData(projectId);
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        taskEndpoint.createTask(session,taskName,projectId);
        System.out.println("TASK HAS BEEN CREATED SUCCESSFULLY");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
