package com.shumov.tm.command.system;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.AdminEndpoint;
import com.shumov.tm.endpoint.Role;
import com.shumov.tm.endpoint.Session;
import com.shumov.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;

public class FasterLoadJSONCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "faster-load-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load all data from .json file by Faster";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOAD FASTER-JSON DATA]");
        if(serviceLocator == null) return;
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.dataLoadFasterJSON(session);
        serviceLocator.setSession(null);
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setRole(Role.GUEST);
        serviceLocator.setCurrentUser(userDTO);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }
}
