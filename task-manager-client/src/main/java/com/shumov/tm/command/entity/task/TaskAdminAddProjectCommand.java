package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskAdminAddProjectCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-add-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Add task in any project";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String projectId = terminalService.nextLine();
        terminalService.isCorrectInputData(projectId);
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = terminalService.nextLine();
        terminalService.isCorrectInputData(taskId);
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.addTaskInProject(session,projectId,taskId);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }
}
