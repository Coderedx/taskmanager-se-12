package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class ProjectAdminEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-edit-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change name of any project";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
//        roleTypes.add(Role.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        terminalService.isCorrectInputData(id);
        System.out.println("ENTER NEW PROJECT NAME:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        terminalService.isCorrectInputData(name);
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.projectEditName(session,id,name);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
