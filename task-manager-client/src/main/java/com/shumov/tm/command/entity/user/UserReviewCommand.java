package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class UserReviewCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-review";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User review";
    }

    @Override
    public void execute() throws Exception {
        reviewUser();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
        roleTypes.add(Role.USER);
    }

    private void reviewUser() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[USER REVIEW]");
        @NotNull final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final UserDTO userDTO = userEndpoint.reviewUser(session);
        System.out.println("Login: "+ userDTO.getLogin());
        System.out.println("Description: "+ userDTO.getDescription() +"\n");
    }
}
