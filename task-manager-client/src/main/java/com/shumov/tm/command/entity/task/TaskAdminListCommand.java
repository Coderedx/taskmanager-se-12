package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.List;

public class TaskAdminListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-list-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all tasks of all users";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK LIST]");
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Task> tasks = adminEndpoint.getTaskList(session);
        for (@NotNull final Task task : tasks) {
            System.out.println("TASK ID: " + task.getId() + "\nTASK NAME: " + task.getName()
                    + "\nPROJECT ID: "+ task.getIdProject()
                    + "\nUSER ID: "+ task.getOwnerId()+"\n");
        }
    }
}
