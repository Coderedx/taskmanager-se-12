package com.shumov.tm.command.entity.user;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class UserAdminReviewCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "admin-review";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "All user review";
    }

    @Override
    public void execute() throws Exception {
        reviewAdmin();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void reviewAdmin() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[USER REVIEW]");
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final UserDTO userDTO = adminEndpoint.reviewUserAdmin(session);
        System.out.println("Login: "+ userDTO.getLogin());
        System.out.println("Role: "+ userDTO.getRole().toString());
        System.out.println("Description: "+ userDTO.getDescription() +"\n");
    }
}
