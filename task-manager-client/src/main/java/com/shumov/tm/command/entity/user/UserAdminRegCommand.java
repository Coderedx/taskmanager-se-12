package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class UserAdminRegCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "admin-reg";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "New admin registration";
    }

    @Override
    public void execute() throws Exception {
        createAdmin();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void createAdmin() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[NEW USER REGISTRATION]");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.println("[ENTER LOGIN FOR NEW ADMIN]");
        @NotNull final String login = terminalService.nextLine();
        terminalService.isCorrectInputData(login);
        System.out.println("[ENTER PASSWORD FOR NEW ADMIN]");
        @NotNull final String pass = terminalService.nextLine();
        terminalService.isCorrectInputData(pass);
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.regAdmin(session,login,pass);
        System.out.println("NEW ADMIN CREATED SUCCESSFULLY");
    }
}
