package com.shumov.tm.command.system;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.AdminEndpoint;
import com.shumov.tm.endpoint.Role;
import com.shumov.tm.endpoint.Session;

import org.jetbrains.annotations.NotNull;

public class JaxBSaveJSONCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "jaxb-save-json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save all data in .json file by JAX-B";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SAVE JSON-XML DATA]");
        if(serviceLocator == null) return;
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.dataSaveJaxBJSON(session);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }
}
