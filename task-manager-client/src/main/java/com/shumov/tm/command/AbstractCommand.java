package com.shumov.tm.command;

import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.endpoint.*;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.Exception;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


@NoArgsConstructor
public abstract class AbstractCommand {

    @NotNull
    protected final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
    @Nullable
    protected ServiceLocator serviceLocator;
    @NotNull
    protected List<Role> roleTypes = new ArrayList<>();
    @NotNull
    public abstract String command();
    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
    @NotNull
    public List<Role> getRoleTypes() {
        return roleTypes;
    }

    public abstract void initRoles();
}
