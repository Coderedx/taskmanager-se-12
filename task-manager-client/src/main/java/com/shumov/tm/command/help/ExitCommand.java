package com.shumov.tm.command.help;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class ExitCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "exit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Exit";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
        roleTypes.add(Role.USER);
        roleTypes.add(Role.GUEST);
    }

}
