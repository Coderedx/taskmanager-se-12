package com.shumov.tm.command.entity.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class ProjectListCommand extends AbstractCommand {

    @NotNull
    private final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    @Override
    @NotNull
    public String command() {
        return "project-list";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects";
    }

    @Override
    public void execute() throws Exception {
        listExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }

    private void listExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT LIST]");
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Project> projects = projectEndpoint.getProjectList(session);
        for (@NotNull final Project project : projects) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName() +
            "\nDATE CREATE: " + project.getDateCreated().toString() +
            "\nDATE START: " + project.getDateStart().toString() +
            "\nDATE FINISH: " + project.getDateFinish().toString() +
                    "\nSTATUS: " + project.getStatus().toString()+"\n");
        }
    }
}
