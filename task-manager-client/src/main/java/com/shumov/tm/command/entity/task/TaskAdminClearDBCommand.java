package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskAdminClearDBCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-clear-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Clear task DB";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[CLEAR DB TASKS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all tasks]".toUpperCase());
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String confirm = terminalService.nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.taskClearDB(session);
        System.out.println("[ALL TASKS HAVE BEEN DELETED SUCCESSFULLY]");
    }
}
