package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;

public class TaskSearchCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "task-search";
    }

    @Override
    public @NotNull String getDescription() {
        return "Task search";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK SEARCH]");
        System.out.println("enter a search term:".toUpperCase());
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String term = terminalService.nextLine();
        terminalService.isCorrectInputData(term);
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Task> list = taskEndpoint.searchTasks(session,term);
        System.out.println("[SEARCH RESULT]");
        for (@NotNull final Task task : list) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName() +
                    "\nDATE CREATE: " + task.getDateCreated().toString() +
                    "\nDATE START: " + task.getDateStart().toString() +
                    "\nDATE FINISH: " + task.getDateFinish().toString() +
                    "\nSTATUS: " + task.getStatus().toString()+"\n");
        }
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
