package com.shumov.tm.command.entity.task;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskClearCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-clear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[CLEAR TASKS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all tasks]".toUpperCase());
        @NotNull final String confirm = serviceLocator.getTerminalService().nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        taskEndpoint.clearTasks(session);
        System.out.println("[ALL TASKS HAVE BEEN DELETED SUCCESSFULLY]");
    }
}
