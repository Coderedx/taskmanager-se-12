package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public final class ProjectClearCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-clear";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all projects";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[CLEAR PROJECTS]");
        System.out.println("[Enter \"Y\" if you confirm deletion of all projects]".toUpperCase());
        ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String confirm = terminalService.nextLine();
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        projectEndpoint.clearProjects(session);
        System.out.println("[ALL PROJECTS HAVE BEEN DELETED SUCCESSFULLY]");
    }

    public void initRoles(){
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}

