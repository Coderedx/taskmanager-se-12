package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class ProjectSetStatus extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "project-set-status";
    }

    @Override
    public @NotNull String getDescription() {
        return "Set project status";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT SET STATUS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        terminalService.isCorrectInputData(id);
        System.out.println("ENTER PROJECT STATUS:");
        System.out.println("planned : project planned");
        System.out.println("progress : project in progress");
        System.out.println("done : project done");
        @NotNull final String status = terminalService.nextLine();
        terminalService.isCorrectInputData(status);
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        projectEndpoint.editProjectStatus(session,id,status);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
