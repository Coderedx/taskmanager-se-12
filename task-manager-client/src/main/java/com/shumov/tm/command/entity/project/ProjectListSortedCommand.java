package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import com.shumov.tm.endpoint.Project;
import com.shumov.tm.endpoint.ProjectEndpoint;
import com.shumov.tm.endpoint.Session;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class ProjectListSortedCommand extends AbstractCommand {

    @NotNull
    private final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    @Override
    public @NotNull String command() {
        return "project-list-sorted";
    }

    @Override
    public @NotNull String getDescription() {
        return "Sorted project list";
    }

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) return;
        System.out.println("[PROJECT SORTED LIST]");
        System.out.println("[How do you want to sort the list?]".toUpperCase());
        System.out.println("create : sort by create date");
        System.out.println("start : sort by start date");
        System.out.println("finish : sort by finish date");
        System.out.println("status : sort by start date");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String method = terminalService.nextLine();
        terminalService.isCorrectInputData(method);
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Project> list = projectEndpoint.getProjectSortedList(session, method);
        for (@NotNull final Project project : list) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName() +
                    "\nDATE CREATE: " + project.getDateCreated().toString() +
                    "\nDATE START: " + project.getDateStart().toString() +
                    "\nDATE FINISH: " + project.getDateFinish().toString() +
                    "\nSTATUS: " + project.getStatus().toString() + "\n");
        }
    }

    @Override
    public void initRoles(){
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }

}
