package com.shumov.tm.command.entity.user;


import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class UserLoginCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "login";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "User login";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String login = terminalService.nextLine();
        terminalService.isCorrectInputData(login);
        System.out.println("ENTER PASSWORD:");
        @NotNull final String pass = terminalService.nextLine();
        terminalService.isCorrectInputData(pass);
        @NotNull final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        @NotNull final Session session = sessionEndpoint.openSession(login,pass);
        serviceLocator.setSession(session);
        @NotNull final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
        @NotNull final UserDTO userDTO = userEndpoint.reviewUser(session);
        serviceLocator.setCurrentUser(userDTO);
        System.out.println("authorization successful".toUpperCase());

    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.GUEST);
    }
}
