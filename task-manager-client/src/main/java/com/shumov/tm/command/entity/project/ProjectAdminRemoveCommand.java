package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class ProjectAdminRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-remove-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove any project by id";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        terminalService.isCorrectInputData(id);
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.removeProjectById(session, id);
        System.out.println("PROJECT HAS BEEN REMOVED SUCCESSFULLY");
    }
}
