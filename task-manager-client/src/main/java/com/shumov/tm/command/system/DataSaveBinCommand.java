package com.shumov.tm.command.system;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.AdminEndpoint;
import com.shumov.tm.endpoint.Role;
import com.shumov.tm.endpoint.Session;
import org.jetbrains.annotations.NotNull;


public class DataSaveBinCommand extends AbstractCommand {
    @Override
    public @NotNull String command() {
        return "data-save-bin";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save all data in .bin file";
    }

    @Override
    public void execute() throws Exception {
        saveData();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void saveData() throws Exception {
        System.out.println("[SAVE BINARY DATA]");
        if(serviceLocator == null) return;
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.dataSaveBin(session);
        System.out.println("[OK]");
    }
}
