package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.List;

public class ProjectTasksCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-tasks";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Review project tasks";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        terminalService.isCorrectInputData(id);
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("[TASK LIST]");
        @NotNull final List<Task> tasks = projectEndpoint.getProjectTasksById(session, id);
        for (@NotNull final Task task : tasks) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName());
        }
    }
}
