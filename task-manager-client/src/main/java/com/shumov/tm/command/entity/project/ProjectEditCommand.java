package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;

import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class ProjectEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit project name";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT PROJECT NAME BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String projectId = terminalService.nextLine();
        terminalService.isCorrectInputData(projectId);
        System.out.println("ENTER NEW PROJECT NAME:");
        @NotNull final String name = serviceLocator.getTerminalService().nextLine();
        terminalService.isCorrectInputData(name);
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        projectEndpoint.editProjectName(session,projectId,name);
        System.out.println("PROJECT NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
