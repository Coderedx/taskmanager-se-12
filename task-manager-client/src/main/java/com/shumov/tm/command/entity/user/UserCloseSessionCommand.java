package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class UserCloseSessionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "logout";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "End current session";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[END CURRENT SESSION]");
        System.out.println("[Enter \"Y\" if you confirm]".toUpperCase());
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String confirm = terminalService.nextLine();
        terminalService.isCorrectInputData(confirm);
        if(!"y".equals(confirm.toLowerCase())){
            System.out.println("[operation has not been confirmed]".toUpperCase());
            return;
        }
        @NotNull final SessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        sessionEndpoint.closeSession(session);
        @NotNull final UserDTO userDTO = serviceLocator.getCurrentUser();
        userDTO.setRole(Role.GUEST);
        userDTO.setLogin(null);
        userDTO.setDescription(null);
        System.out.println("Current session completed successfully".toUpperCase());
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
