package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class UserRegCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-reg";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "New user registration";
    }

    @Override
    public void execute() throws Exception {
        createUser();
    }

    private void createUser() throws Exception  {
        if(serviceLocator == null) return;
        System.out.println("[NEW USER REGISTRATION]");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        System.out.println("[ENTER LOGIN FOR NEW USER]");
        @NotNull final String login = terminalService.nextLine();
        terminalService.isCorrectInputData(login);
        System.out.println("[ENTER PASSWORD FOR NEW USER]");
        @NotNull final String pass = terminalService.nextLine();
        terminalService.isCorrectInputData(pass);
        @NotNull final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
        userEndpoint.createNewUser(login,pass);
        System.out.println("NEW USER CREATED SUCCESSFULLY");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
        roleTypes.add(Role.GUEST);
    }
}
