package com.shumov.tm.command.help;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "help";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all commands";
    }

    @Override
    public void execute() throws Exception {
        for (@NotNull final AbstractCommand command : checkCommandsList()) {
            System.out.println(command.command() + ": " + command.getDescription());
        }
    }

    private List<AbstractCommand> checkCommandsList(){
        @NotNull final List<AbstractCommand> list = new ArrayList<>();
        if(serviceLocator==null) return list;
        @NotNull final Role role = serviceLocator.getCurrentUser().getRole();
        for (@NotNull final AbstractCommand abstractCommand : serviceLocator.getCommands().values()){
            if(abstractCommand.getRoleTypes().contains(role)){
                list.add(abstractCommand);
            }
        }
        return list;
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
        roleTypes.add(Role.USER);
        roleTypes.add(Role.GUEST);
    }
}
