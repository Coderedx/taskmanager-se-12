package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit task name";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String taskId = terminalService.nextLine();
        terminalService.isCorrectInputData(taskId);
        System.out.println("ENTER NEW TASK NAME:");
        @NotNull final String taskName = terminalService.nextLine();
        terminalService.isCorrectInputData(taskName);
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        taskEndpoint.editTaskName(session,taskId,taskName);
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}

