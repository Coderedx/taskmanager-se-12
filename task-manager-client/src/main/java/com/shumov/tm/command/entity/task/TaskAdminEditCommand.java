package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskAdminEditCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-edit-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Change name of any task";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT TASK NAME BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String taskId = terminalService.nextLine();
        terminalService.isCorrectInputData(taskId);
        System.out.println("ENTER NEW TASK NAME:");
        @NotNull final String taskName = serviceLocator.getTerminalService().nextLine();
        terminalService.isCorrectInputData(taskName);
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.taskEditName(session, taskId, taskName);
        System.out.println("TASK NAME HAS BEEN CHANGED SUCCESSFULLY");
    }
}
