package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.List;

public class ProjectAdminTasksCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-tasks-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Review all tasks";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REVIEW PROJECT TASKS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        terminalService.isCorrectInputData(id);
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Task> tasks = adminEndpoint.getTaskList(session);
        System.out.println("[TASK LIST]");
        for (@NotNull final Task task : tasks) {
            System.out.println("TASK ID: " + task.getId() + "\nTASK NAME: " + task.getName()+
                    "\nUSER ID: "+ task.getOwnerId()+"\n");
        }
    }
}
