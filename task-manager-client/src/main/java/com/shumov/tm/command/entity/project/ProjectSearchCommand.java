package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.List;

public class ProjectSearchCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "project-search";
    }

    @Override
    public @NotNull String getDescription() {
        return "Project search";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT SEARCH]");
        System.out.println("enter a search term:".toUpperCase());
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String term = terminalService.nextLine();
        terminalService.isCorrectInputData(term);
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Project> listProjects = projectEndpoint.searchProjects(session, term);
        System.out.println("[SEARCH RESULT]");
        for (@NotNull final Project project : listProjects) {
            System.out.println("PROJECT ID: " + project.getId() + " PROJECT NAME: " + project.getName() +
                    "\nDATE CREATE: " + project.getDateCreated().toString() +
                    "\nDATE START: " + project.getDateStart().toString() +
                    "\nDATE FINISH: " + project.getDateFinish().toString() +
                    "\nSTATUS: " + project.getStatus().toString()+"\n");
        }
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
