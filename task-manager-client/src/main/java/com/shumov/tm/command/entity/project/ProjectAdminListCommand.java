package com.shumov.tm.command.entity.project;

import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.util.List;

public class ProjectAdminListCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-list-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show all projects of all users";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT LIST]");
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Project> projects = adminEndpoint.getProjectList(session);
        for (@NotNull final Project project : projects) {
            System.out.println("PROJECT ID: " + project.getId() +"\n"
                    + "PROJECT NAME: " + project.getName() + "\n" +
                    "USER ID: " + project.getOwnerId()+ "\n");
        }
    }
}
