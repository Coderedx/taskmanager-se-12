package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class ProjectSetDateCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "project-set-date";
    }

    @Override
    public @NotNull String getDescription() {
        return "Project set date";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT SET DATE]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String id = terminalService.nextLine();
        terminalService.isCorrectInputData(id);
        System.out.println("ENTER START DATE dd.MM.yyyy:");
        @NotNull final String dateStart = terminalService.nextLine();
        terminalService.isCorrectInputData(dateStart);
        System.out.println("ENTER FINISH DATE dd.MM.yyyy:");
        @NotNull final String dateFinish = terminalService.nextLine();
        terminalService.isCorrectInputData(dateFinish);
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        projectEndpoint.editProjectDate(session,id,dateStart,dateFinish);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
