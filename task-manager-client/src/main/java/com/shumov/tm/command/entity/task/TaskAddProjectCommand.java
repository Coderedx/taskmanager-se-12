package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskAddProjectCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-add";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Add task in project";
    }

    @Override
    public void execute() throws Exception {
        userExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }

    private void userExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[ADD TASK IN PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String projectId = terminalService.nextLine();
        terminalService.isCorrectInputData(projectId);
        @NotNull final TaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint().getTaskEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = terminalService.nextLine();
        terminalService.isCorrectInputData(taskId);
        taskEndpoint.addTaskInProject(session,projectId,taskId);
        System.out.println("TASK HAS BEEN ADD SUCCESSFULLY");
    }
}
