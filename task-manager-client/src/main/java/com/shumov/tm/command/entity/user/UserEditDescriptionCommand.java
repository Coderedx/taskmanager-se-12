package com.shumov.tm.command.entity.user;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class UserEditDescriptionCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "user-edit";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Edit user description";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[EDIT USER DESCRIPTION]");
        System.out.println("ENTER NEW DESCRIPTION:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String description = terminalService.nextLine();
        terminalService.isCorrectInputData(description);
        @NotNull final UserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        userEndpoint.editDescription(session,description);
        System.out.println("Description changed successfully".toUpperCase());
    }


    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
