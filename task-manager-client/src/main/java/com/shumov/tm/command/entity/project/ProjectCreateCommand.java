package com.shumov.tm.command.entity.project;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "project-create";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[PROJECT CREATE]\nENTER PROJECT NAME:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String name = terminalService.nextLine();
        terminalService.isCorrectInputData(name);
        @NotNull final ProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint().getProjectEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        projectEndpoint.createProject(session, name);
        System.out.println("PROJECT HAS BEEN CREATED SUCCESSFULLY");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
