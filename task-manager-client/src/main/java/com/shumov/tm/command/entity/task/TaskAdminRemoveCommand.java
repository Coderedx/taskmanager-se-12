package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskAdminRemoveCommand extends AbstractCommand {

    @Override
    @NotNull
    public String command() {
        return "task-remove-admin";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove any task by id";
    }

    @Override
    public void execute() throws Exception {
        adminExecute();
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.ADMIN);
    }

    private void adminExecute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String taskId = terminalService.nextLine();
        terminalService.isCorrectInputData(taskId);
        @NotNull final AdminEndpoint adminEndpoint = serviceLocator.getAdminEndpoint().getAdminEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        adminEndpoint.removeTaskById(session,taskId);
        System.out.println("TASK HAS BEEN REMOVED SUCCESSFULLY");
    }
}
