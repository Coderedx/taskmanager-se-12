package com.shumov.tm.command.entity.task;


import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;


public class TaskListSortedCommand extends AbstractCommand {

    @NotNull
    private final DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");

    @Override
    public @NotNull String command() {
        return "task-list-sorted";
    }

    @Override
    public @NotNull String getDescription() {
        return "Sorted task list";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK SORTED LIST]");
        System.out.println("[How do you want to sort the list?]".toUpperCase());
        System.out.println("create : sort by create date");
        System.out.println("start : sort by start date");
        System.out.println("finish : sort by finish date");
        System.out.println("status : sort by start date");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String method = terminalService.nextLine();
        terminalService.isCorrectInputData(method);
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        @NotNull final List<Task> list = taskEndpoint.getTaskSortedList(session, method);
        for (@NotNull final Task task : list) {
            System.out.println("TASK ID: " + task.getId() + " TASK NAME: " + task.getName() +
                    "\nPROJECT ID: "+task.getIdProject()+
                    "\nDATE CREATE: " + task.getDateCreated().toString() +
                    "\nDATE START: " + task.getDateStart().toString() +
                    "\nDATE FINISH: " + task.getDateFinish().toString() +
                    "\nSTATUS: " + task.getStatus().toString()+"\n");
        }
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
