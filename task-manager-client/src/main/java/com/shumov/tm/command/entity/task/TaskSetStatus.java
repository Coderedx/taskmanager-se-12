package com.shumov.tm.command.entity.task;

import com.shumov.tm.api.service.ITerminalService;
import com.shumov.tm.command.AbstractCommand;
import com.shumov.tm.endpoint.*;
import org.jetbrains.annotations.NotNull;

import java.lang.Exception;

public class TaskSetStatus extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "task-set-status";
    }

    @Override
    public @NotNull String getDescription() {
        return "Set task status";
    }

    @Override
    public void execute() throws Exception {
        if(serviceLocator == null) return;
        System.out.println("[TASK SET STATUS]");
        System.out.println("ENTER TASK ID:");
        @NotNull final ITerminalService terminalService = serviceLocator.getTerminalService();
        @NotNull final String taskId = terminalService.nextLine();
        terminalService.isCorrectInputData(taskId);
        System.out.println("ENTER TASK STATUS:");
        System.out.println("planned : task planned");
        System.out.println("progress : task in progress");
        System.out.println("done : task done");
        @NotNull final String status = terminalService.nextLine();
        terminalService.isCorrectInputData(status);
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        @NotNull final Session session = serviceLocator.getSession();
        taskEndpoint.editTaskStatus(session,taskId,status);
        System.out.println("[OK]");
    }

    @Override
    public void initRoles() {
        roleTypes.add(Role.USER);
        roleTypes.add(Role.ADMIN);
    }
}
