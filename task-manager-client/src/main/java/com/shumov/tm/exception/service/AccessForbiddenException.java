package com.shumov.tm.exception.service;

public class AccessForbiddenException extends Exception {

    public AccessForbiddenException() {
        super("[Access Forbidden]");
    }
}
