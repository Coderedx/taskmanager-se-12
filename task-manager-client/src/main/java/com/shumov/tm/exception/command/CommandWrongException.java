package com.shumov.tm.exception.command;

public class CommandWrongException extends Exception {

    public CommandWrongException() {
        super("Wrong command!");
    }
}
