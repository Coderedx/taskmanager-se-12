package com.shumov.tm.endpoint;

import com.shumov.tm.api.service.IProjectService;
import com.shumov.tm.api.service.ISessionService;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.Task;
import com.shumov.tm.exception.endpoint.EndpointServiceNotInitialized;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService
public class ProjectEndpoint {

    @Nullable
    private IProjectService projectService;
    private ISessionService sessionService;

    public ProjectEndpoint(@Nullable final IProjectService projectService,
                           @Nullable final ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void createProject(
        @WebParam(name = "session")@Nullable final Session session,
        @WebParam(name = "name")@Nullable final String name
    ) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        projectService.createProject(session.getUserId(), name);
    }

    @NotNull
    @WebMethod
    public List<Project> getProjectList(
            @WebParam(name = "session")@Nullable final Session session) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        return projectService.getProjectList(session.getUserId());
    }

    @NotNull
    @WebMethod
    public Project getProject(
            @WebParam(name = "session")@Nullable final Session session,
            @WebParam(name = "projectId")@Nullable final String projectId) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        return projectService.getProject(session.getUserId(), projectId);
    }

    @WebMethod
    public void editProjectName(
            @WebParam(name = "session")@Nullable final Session session,
            @WebParam(name = "projectId")@Nullable final String projectId,
            @WebParam(name = "name")@Nullable final String name
    ) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        projectService.editProjectNameById(session.getUserId(), projectId, name);
    }

    @WebMethod
    public void editProjectDate(
            @WebParam(name = "session")@Nullable final Session session,
            @WebParam(name = "projectId")@Nullable final String projectId,
            @WebParam(name = "start")@Nullable final String start,
            @WebParam(name = "finish")@Nullable final String finish
    ) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        projectService.editProjectDate(session.getUserId(), projectId, start, finish);
    }

    @WebMethod
    public void editProjectStatus(
            @WebParam(name = "session")@Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "status") @NotNull final String status
    ) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        projectService.editProjectStatus(session.getUserId(), projectId, status);
    }

    @WebMethod
    public void removeProject(
            @WebParam(name = "session")@Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if(projectService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        projectService.removeProjectById(session.getUserId(), projectId);
    }

    @WebMethod
    public void clearProjects(
            @WebParam(name = "session")@Nullable final Session session) throws Exception {
        if(projectService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        projectService.clearProjects(session.getUserId());
    }

    @NotNull
    @WebMethod
    public List<Project> searchProjects(
            @WebParam(name = "session")@Nullable final Session session,
            @WebParam(name = "term")@NotNull final String term) throws Exception {
        if(projectService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        return projectService.searchProjects(session.getUserId(), term);
    }

    @NotNull
    @WebMethod
    public List<Project> getProjectSortedList(
            @WebParam(name = "session")@Nullable final Session session,
            @WebParam(name = "term") @NotNull final String method) throws Exception {
        if(projectService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        return projectService.getProjectSortedList(session.getUserId(), method);
    }

    @NotNull
    @WebMethod
    public List<Task> getProjectTasksById(
            @WebParam(name = "session")@Nullable final Session session,
            @WebParam(name = "projectId")@NotNull final String projectId) throws Exception {
        if(projectService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        return projectService.getProjectTasksById(session.getUserId(), projectId);
    }
}
