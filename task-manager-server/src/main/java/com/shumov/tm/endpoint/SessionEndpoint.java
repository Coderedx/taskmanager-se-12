package com.shumov.tm.endpoint;

import com.shumov.tm.api.service.ISessionService;
import com.shumov.tm.entity.Session;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.endpoint.EndpointServiceNotInitialized;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class SessionEndpoint {

    @Nullable
    private ISessionService sessionService;

    public SessionEndpoint(@Nullable ISessionService sessionService) {
        this.sessionService = sessionService;
    }

    @NotNull
    @WebMethod
    public Session openSession(
            @WebParam(name = "login")@Nullable final String login,
            @WebParam(name = "password")@Nullable final String password) throws Exception {
        if(sessionService == null) throw new EndpointServiceNotInitialized();
        return sessionService.openSession(login, password);
    }

    @WebMethod
    public void closeSession(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        sessionService.closeSession(session);
    }
}
