package com.shumov.tm.endpoint;

import com.shumov.tm.api.service.ISessionService;
import com.shumov.tm.api.service.ITaskService;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.Task;
import com.shumov.tm.enumerate.ExecutionStatus;
import com.shumov.tm.exception.endpoint.EndpointServiceNotInitialized;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class TaskEndpoint {

    @Nullable
    private ITaskService taskService;
    private ISessionService sessionService;

    public TaskEndpoint(@Nullable ITaskService taskService,
                        @Nullable ISessionService sessionService) {
        this.taskService = taskService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void createTask(
        @WebParam(name = "session") @Nullable final Session session,
        @WebParam(name = "name") @Nullable final String name,
        @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        taskService.createTask(session.getUserId(), name, projectId);
    }

    @NotNull
    @WebMethod
    public List<Task> getTaskList(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        return taskService.getTaskList(session.getUserId());
    }

    @NotNull
    @WebMethod
    public Task getTask( @WebParam(name = "session") @Nullable final Session session,
                         @WebParam(name = "taskId")  @Nullable final String taskId) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        return taskService.getTask(session.getUserId(), taskId);
    }

    @NotNull
    @WebMethod
    public List<Task> getProjectTasks( @WebParam(name = "session") @Nullable final Session session,
                                       @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        return taskService.getTasksOfProject(session.getUserId(), projectId);
    }

    @WebMethod
    public void clearTasks( @WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        taskService.clearTasks(session.getUserId());
    }

    @WebMethod
    public void editTaskName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId") @Nullable final String taskId,
            @WebParam(name = "name") @Nullable final String name
    ) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        taskService.editTaskNameById(session.getUserId(), taskId, name);
    }

    @WebMethod
    public void editTaskDate(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId")@Nullable final String taskId,
            @WebParam(name = "start")@Nullable final String start,
            @WebParam(name = "finish")@Nullable final String finish
    ) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        taskService.editTaskDate(session.getUserId(), taskId, start, finish);
    }

    @WebMethod
    public void editTaskStatus(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId") @Nullable final String taskId,
            @WebParam(name = "status") @Nullable final String status
    ) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        taskService.editTaskStatus(session.getUserId(), taskId, status);
    }

    @WebMethod
    public void removeTaskById(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "taskId") @Nullable final String taskId) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        taskService.removeTaskById(session.getUserId(), taskId);
    }

    @WebMethod
    public void addTaskInProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        taskService.addTaskInProject(session.getUserId(), projectId, taskId);
    }

    @NotNull
    @WebMethod
    public List<Task> getTaskSortedList(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "method") @Nullable final String method) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        return taskService.getTaskSortedList(session.getUserId(), method);
    }

    @NotNull
    @WebMethod
    public List<Task> searchTasks(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "term") @Nullable final String term) throws Exception {
        if(taskService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        return taskService.searchTasks(session.getUserId(), term);
    }
}
