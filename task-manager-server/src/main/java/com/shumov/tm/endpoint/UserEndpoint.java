package com.shumov.tm.endpoint;

import com.shumov.tm.api.service.ISessionService;
import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.User;
import com.shumov.tm.entity.dto.UserDTO;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.endpoint.EndpointServiceNotInitialized;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class UserEndpoint {

    @Nullable
    private IUserService userService;
    private ISessionService sessionService;

    public UserEndpoint(@Nullable IUserService userService,
                        @Nullable ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void createNewUser( @WebParam(name = "login") @NotNull final String login,
                               @WebParam(name = "password") @NotNull final String password) throws Exception {
        if(userService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        userService.createNewUser(login, password);
    }
    @WebMethod
    public void editDescription(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "description") @NotNull final String description) throws Exception {
        if(userService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        userService.editDescription(session.getUserId(), description);
    }
    @WebMethod
    public void editPassword(@WebParam(name = "session") @Nullable final Session session,
                             @WebParam(name = "password") @NotNull final String password) throws Exception {
        if(userService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session);
        userService.editPassword(session.getUserId(), password);
    }

    @WebMethod
    public UserDTO reviewUser(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        sessionService.validate(session);
        return userService.reviewUser(session.getUserId());
    }
}
