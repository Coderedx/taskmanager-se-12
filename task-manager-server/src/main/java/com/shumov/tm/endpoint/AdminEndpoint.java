package com.shumov.tm.endpoint;

import com.shumov.tm.api.service.IAdminService;
import com.shumov.tm.api.service.ISessionService;
import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.dto.UserDTO;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.endpoint.EndpointServiceNotInitialized;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class AdminEndpoint {

    private IAdminService adminService;
    private ISessionService sessionService;

    public AdminEndpoint(@Nullable final IAdminService adminService,
                         @Nullable final ISessionService sessionService) {
        this.adminService = adminService;
        this.sessionService = sessionService;
    }

    @WebMethod
    public void projectClearDB(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.projectClearDB();
    }

    @WebMethod
    public void projectEditName(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "projectId") @Nullable final String projectId,
                                @WebParam(name = "name") @Nullable final String name) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.projectEditName(projectId, name);
    }

    @WebMethod
    public @NotNull List<Project> getProjectList(@WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        return adminService.getProjectList();
    }

    @WebMethod
    public void removeProjectById(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "projectId") @Nullable final String projectId) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.removeProjectById(projectId);
    }

    @WebMethod
    public @NotNull List<Task> getProjectTasksById(@WebParam(name = "session") @Nullable final Session session,
                                                   @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        return adminService.getProjectTasksById(projectId);
    }

    @WebMethod
    public void addTaskInProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId") @Nullable final String taskId) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.addTaskInProject(projectId, taskId);
    }

    @WebMethod
    public void taskClearDB(@WebParam(name = "session")@Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.taskClearDB();
    }

    @WebMethod
    public void taskEditName(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId") @Nullable final String taskId,
            @WebParam(name = "name") @Nullable final String name) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.taskEditName(taskId, name);
    }

    @WebMethod
    public @NotNull List<Task> getTaskList(@WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        return adminService.getTaskList();
    }

    @WebMethod
    public void removeTaskById(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "session") @Nullable final String taskId) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.removeTaskById(taskId);
    }

    @WebMethod
    public void regAdmin(@WebParam(name = "session") @Nullable final Session session,
                         @WebParam(name = "login") @Nullable final String login,
                         @WebParam(name = "pass") @Nullable final String pass) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.regAdmin(login, pass);
    }

    @WebMethod
    public @NotNull UserDTO reviewUserAdmin(
            @WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        return adminService.reviewUser(session);
    }

    @WebMethod
    public void dataLoadBin(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataLoadBin();
    }

    @WebMethod
    public void dataSaveBin(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataSaveBin();
    }

    @WebMethod
    public void dataLoadFasterJSON(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataLoadFasterJSON();
    }

    @WebMethod
    public void dataLoadFasterXML(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataLoadFasterXML();
    }

    @WebMethod
    public void dataSaveFasterJSON(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataSaveFasterJSON();
    }

    @WebMethod
    public void dataSaveFasterXML(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataSaveFasterXML();
    }

    @WebMethod
    public void dataLoadJaxBJSON(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataLoadJaxBJSON();
    }

    @WebMethod
    public void dataLoadJaxBXML(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataLoadJaxBXML();
    }

    @WebMethod
    public void dataSaveJaxBJSON(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataSaveJaxBJSON();
    }

    @WebMethod
    public void dataSaveJaxBXML(@WebParam(name = "session") @Nullable final Session session) throws Exception {
        if(adminService == null || sessionService == null) throw new EndpointServiceNotInitialized();
        sessionService.validate(session, Role.ADMIN);
        adminService.dataSaveJaxBXML();
    }
}
