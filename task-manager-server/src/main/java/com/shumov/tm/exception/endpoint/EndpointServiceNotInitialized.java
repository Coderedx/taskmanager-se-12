package com.shumov.tm.exception.endpoint;


public class EndpointServiceNotInitialized extends Exception {

    public EndpointServiceNotInitialized() {
        super("Service not initialized");
    }
}
