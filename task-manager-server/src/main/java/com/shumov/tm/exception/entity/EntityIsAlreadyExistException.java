package com.shumov.tm.exception.entity;

public class EntityIsAlreadyExistException extends Exception {

    public EntityIsAlreadyExistException() {
        super("OBJECT WITH THIS ID IS ALREADY EXIST!");
    }
}
