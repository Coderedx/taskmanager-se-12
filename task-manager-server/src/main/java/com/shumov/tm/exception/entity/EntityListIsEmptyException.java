package com.shumov.tm.exception.entity;

public class EntityListIsEmptyException extends Exception {

    public EntityListIsEmptyException() {
        super("The list of objects is empty!".toUpperCase());
    }
}
