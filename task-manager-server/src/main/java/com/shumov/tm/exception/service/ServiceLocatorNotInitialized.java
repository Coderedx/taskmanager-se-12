package com.shumov.tm.exception.service;

public class ServiceLocatorNotInitialized extends Exception {

    public ServiceLocatorNotInitialized() {
        super("Service Locator not initialized");
    }
}
