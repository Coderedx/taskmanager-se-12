package com.shumov.tm.service;

import com.shumov.tm.api.repository.IUserRepository;
import com.shumov.tm.api.service.IUserService;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.User;
import com.shumov.tm.entity.dto.UserDTO;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.server.ServerException;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import com.shumov.tm.repository.UserRepository;
import com.shumov.tm.util.ConnectorDBUtil;
import com.shumov.tm.util.HashUtil;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebService;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.List;

@NoArgsConstructor
@WebService
public class UserService extends AbstractService implements IUserService {

    @Nullable
    private IUserRepository repository;
    @Nullable
    private ServiceLocator serviceLocator;
    @Nullable
    private Connection connection = ConnectorDBUtil.connectionDB();

    public UserService(@Nullable ServiceLocator serviceLocator, @Nullable IUserRepository repository) {
        repository.setConnection(connection);
        this.repository = repository;
        this.serviceLocator = serviceLocator;
    }

    @Override
    @NotNull
    public List<User> getUserList() throws Exception {
        try {
            return repository.findAll();
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    @NotNull
    public final User getUser(@Nullable final String id) throws Exception{
        try {
            isCorrectInputData(id);
            return repository.findOne(id);
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    @Override
    public final void createNewUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        try {
            isCorrectLogin(login);
            isCorrectPass(password);
            @NotNull final String passwordHash = HashUtil.getMd5(password);
            @NotNull final User user = new User(login, passwordHash);
            if (repository == null) return;
            repository.persist(user);
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    @Override
    public final void createNewUser(
            @Nullable final String login,
            @Nullable final String password,
            @NotNull final Role userRole
    ) throws Exception {
        try {
            isCorrectLogin(login);
            isCorrectPass(password);
            @NotNull final String passwordHash = HashUtil.getMd5(password);
            @NotNull final User user = new User(login, passwordHash, userRole);
            if (repository == null) return;
            repository.persist(user);
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    @Override
    @NotNull
    public final User getUserByLogin(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception {
        try {
            if (repository == null) throw new ServiceLocatorNotInitialized();
            isCorrectLogin(login);
            isCorrectPass(password);
            @NotNull final String passwordHash = HashUtil.getMd5(password);
            @Nullable final User user = repository.findOneByLogin(login);
            if (user == null) throw new IOException("Wrong login or password");
            @Nullable final String userPasswordHash = user.getPassword();
            if (passwordHash.equals(userPasswordHash)) return user;
            else throw new IOException("Wrong Password");
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    @Override
    public final void mergeUser(@Nullable final User user) throws Exception {
        try {
            isCorrectObject(user);
            @NotNull final String id = user.getId();
            repository.merge(id, user);
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    @NotNull
    @Override
    public UserDTO reviewUser(@NotNull final String userId) throws Exception {
        @NotNull final User user = getUser(userId);
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(user.getLogin());
        userDTO.setRole(user.getRole());
        userDTO.setDescription(user.getDescription());
        return userDTO;
    }

    @Override
    public final void editDescription(@NotNull final String userId,
                                      @NotNull final String description) throws Exception{
        try {
            isCorrectInputData(userId);
            isCorrectInputData(description);
            @NotNull final User user = repository.findOne(userId);
            user.setDescription(description);
            mergeUser(user);
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    @Override
    public final void editPassword(@NotNull final String userId, @NotNull final String password) throws Exception  {
        @NotNull final User user = getUser(userId);
        @Nullable final String passwordHash = HashUtil.getMd5(password);
        user.setPassword(HashUtil.getMd5(password));
        mergeUser(user);
    }

    @Override
    public void setUsers(@NotNull final List<User> users) throws Exception{
        try {
            repository.removeAll();
            for (@NotNull final User user : users) {
                isCorrectObject(user);
                repository.persist(user);
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    public int checkCount() throws Exception{
        try {
            return repository.checkCount();
        } catch (Exception e){
            e.printStackTrace();
            throw new ServerException(e.getMessage());
        }
    }

    @Override
    public final void isCorrectLogin(@Nullable final String login) throws IOException {
        if (login==null || login.isEmpty()){
            throw new IOException("Incorrect login!".toUpperCase());
        }
    }

    @Override
    public final void isCorrectPass(@Nullable final String pass) throws IOException{
        if(pass==null || pass.isEmpty()){
            throw new IOException("Incorrect password!".toUpperCase());
        }
    }

    @Override
    public final void isCorrectDescription(@Nullable final String description) throws IOException {
        if(description==null || description.isEmpty()){
            throw new IOException("Incorrect description!".toUpperCase());
        }
    }
}
