package com.shumov.tm.service;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.api.service.Service;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@NoArgsConstructor
public abstract class AbstractService implements Service {

    @Override
    public final void isCorrectInputData(@Nullable final String name) throws Exception {
        if (name==null || name.isEmpty()){
            throw new IOException("Incorrect data entered".toUpperCase());
        }
    }

    @Override
    public final void isCorrectObject(@Nullable final Entity entity) throws Exception {
        if (entity==null){
            throw new IOException("Incorrect object".toUpperCase());
        }
    }

    public final @NotNull Date parseDate (@Nullable final String date) throws IOException{
        @NotNull final DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            @NotNull final Date formattedDate = dateFormat.parse(date);
            return formattedDate;
        } catch (ParseException e){
            throw new IOException("Wrong date format");
        }
    }
}
