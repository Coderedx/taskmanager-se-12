package com.shumov.tm.bootstrap;



import com.shumov.tm.api.repository.IProjectRepository;
import com.shumov.tm.api.repository.ITaskRepository;
import com.shumov.tm.api.repository.IUserRepository;
import com.shumov.tm.api.service.*;
import com.shumov.tm.endpoint.*;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.repository.ProjectRepository;
import com.shumov.tm.repository.SessionRepository;
import com.shumov.tm.repository.TaskRepository;
import com.shumov.tm.repository.UserRepository;
import com.shumov.tm.service.*;
import com.shumov.tm.util.ConnectorDBUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.validation.constraints.Null;
import javax.xml.ws.Endpoint;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.*;

@NoArgsConstructor
@Getter
@Setter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private SessionRepository sessionRepository = new SessionRepository();
    @NotNull
    private IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(this, projectRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(this, taskRepository);
    @NotNull
    private final IUserService userService = new UserService(this, userRepository);
    @NotNull
    private final IDomainService domainService = new DomainService(this);
    @NotNull
    private final ISessionService sessionService = new SessionService(this, sessionRepository);
    @NotNull
    private final IAdminService adminService = new AdminService(this);
    @Nullable
    private Connection connectionDB;

    public void init() throws Exception {
        initConnection();
//        initUsers();
        publishEndpoints();
        System.out.println(userService.checkCount());
    }

    private void publishEndpoints(){
        Endpoint.publish("http://localhost:8080/ProjectEndpoint?wsdl",
                new ProjectEndpoint(projectService, sessionService));
        Endpoint.publish("http://localhost:8080/TaskEndpoint?wsdl",
                new TaskEndpoint(taskService, sessionService));
        Endpoint.publish("http://localhost:8080/UserEndpoint?wsdl",
                new UserEndpoint(userService, sessionService));
        Endpoint.publish("http://localhost:8080/SessionEndpoint?wsdl",
                new SessionEndpoint(sessionService));
        Endpoint.publish("http://localhost:8080/AdminEndpoint?wsdl",
                new AdminEndpoint(adminService, sessionService));
        System.out.println("http://localhost:8080/ProjectEndpoint?wsdl");
        System.out.println("http://localhost:8080/TaskEndpoint?wsdl");
        System.out.println("http://localhost:8080/UserEndpoint?wsdl");
        System.out.println("http://localhost:8080/SessionEndpoint?wsdl");
        System.out.println("http://localhost:8080/AdminEndpoint?wsdl");

    }

    private void initConnection() throws Exception {
        connectionDB = ConnectorDBUtil.connectionDB();
    }

    private void initUsers() throws Exception  {
        userService.createNewUser("user", "user", Role.USER);
        userService.createNewUser("admin", "admin", Role.ADMIN);
    }
}
