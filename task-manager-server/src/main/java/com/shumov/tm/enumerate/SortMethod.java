package com.shumov.tm.enumerate;

public enum SortMethod {

    BY_CREATE_DATE,
    BY_START_DATE,
    BY_FINISH_DATE,
    BY_STATUS,

}
