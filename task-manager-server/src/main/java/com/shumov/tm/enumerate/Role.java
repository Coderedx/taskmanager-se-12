package com.shumov.tm.enumerate;

import org.jetbrains.annotations.NotNull;

public enum Role {

    ADMIN("Admin"),
    USER("User"),
    GUEST("Guest");

    @NotNull
    String displayName;

    Role(@NotNull String displayName){
        this.displayName = displayName;
    }

    @Override
    @NotNull
    public String toString() {
        return displayName;
    }
}
