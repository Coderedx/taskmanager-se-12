package com.shumov.tm.enumerate;

import org.jetbrains.annotations.NotNull;

public enum ExecutionStatus {

    PLANNED("Planned",0),
    PROGRESS("Progress",1),
    DONE("Done",2);

    @NotNull
    String displayName;
    int status;

    ExecutionStatus(@NotNull String displayName, int status){
        this.displayName = displayName;
        this.status = status;
    }

    @Override
    @NotNull
    public String toString() {
        return displayName;
    }

    public int getNumber() {
        return status;
    }
}
