package com.shumov.tm.repository;

import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.api.repository.IRepository;
import com.shumov.tm.api.service.ServiceLocator;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public abstract class AbstractRepository<T extends Entity> implements IRepository<T> {

    @Nullable
    @Setter
    protected Connection connection;
}
