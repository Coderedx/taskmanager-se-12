package com.shumov.tm.repository;

import com.shumov.tm.api.repository.IUserRepository;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.entity.User;
import com.shumov.tm.enumerate.Role;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    private final int ID_FIELD = 1;
    private final int LOGIN_FIELD = 2;
    private final int DESCRIPTION_FIELD = 3;
    private final int PASSWORD_HASH_FIELD = 4;
    private final int ROLE_FIELD = 5;

    private final String tableName = "app_user";

    @Override
    @Nullable
    public final User findOne(@NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable final User user = fetch(resultSet);
        statement.close();
        if (user == null) return null;
        else return user;
    }

    @Nullable
    public final User findOneByLogin(@NotNull final String login) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE login=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable final User user = fetch(resultSet);
        statement.close();
        if (user == null) return null;
        else return user;
    }

    @Override
    @NotNull
    public List<User> findAll() throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final List<User> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + tableName + ";";
        @NotNull final Statement statement = connection.createStatement();
        @NotNull ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    public final void persist(@NotNull final User user) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "INSERT INTO " + tableName + " VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getId());
        statement.setString(2, user.getLogin());
        statement.setString(3, user.getDescription());
        statement.setString(4, user.getPassword());
        statement.setString(5, user.getRole().toString());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final String entityId, @NotNull final User user) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "UPDATE " + tableName + " SET login=?, description=?, passwordHash=?, " +
                "role=? WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getDescription());
        statement.setString(3, user.getPassword());
        statement.setString(4, user.getRole().toString());
        statement.setString(5, entityId);
        statement.executeUpdate();
        statement.close();

    }

    @Override
    public void remove(@NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName + " WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        statement.executeUpdate();
        statement.close();
    }

    public int checkCount() throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT COUNT(id) AS count FROM " + tableName;
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        int count = 0;
        if (resultSet.next()) count = resultSet.getInt(1);
        statement.close();
        return count;
    }


    @Override
    public void removeAll() throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName;
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final User user = new User();
        row.next();
        user.setId(row.getString(ID_FIELD));
        user.setLogin(row.getString(LOGIN_FIELD));
        user.setDescription(row.getString(DESCRIPTION_FIELD));
        user.setPassword(row.getString(PASSWORD_HASH_FIELD));
        @Nullable final String status = row.getString(ROLE_FIELD).toUpperCase();
        @Nullable Role role;
        try {
            role = Role.valueOf(status);
        } catch (IllegalArgumentException e) {
            role = Role.GUEST;
        }
        user.setRole(role);
        return user;
    }
}
