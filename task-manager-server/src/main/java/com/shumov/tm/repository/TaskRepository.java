package com.shumov.tm.repository;

import com.shumov.tm.api.repository.ITaskRepository;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.entity.Task;
import com.shumov.tm.enumerate.ExecutionStatus;
import com.shumov.tm.exception.entity.EntityIsAlreadyExistException;
import com.shumov.tm.exception.entity.EntityListIsEmptyException;
import com.shumov.tm.exception.entity.EntityNotExistException;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    private final int ID_FIELD = 1;
    private final int DATE_START_FIELD = 2;
    private final int DATE_FINISH_FIELD = 3;
    private final int DATE_CREATE_FIELD = 4;
    private final int DESCRIPTION_FIELD = 5;
    private final int NAME_FIELD = 6;
    private final int PROJECT_ID_FIELD = 7;
    private final int OWNER_ID_FIELD = 8;
    private final int STATUS_FIELD = 9;

    private final String tableName = "app_task";

    @Override
    @NotNull
    public List<Task> findAll() throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + tableName;
        @NotNull final Statement statement = connection.createStatement();
        @NotNull ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String ownerId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE user_id=?";
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, ownerId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        statement.close();
        if (task == null) return null;
        else return task;
    }

    @Override
    @Nullable
    public Task findOne(
            @NotNull final String ownerId,
            @NotNull final String entityId
    ) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE id=? AND user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        statement.setString(2, ownerId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        statement.close();
        if (task == null) return null;
        else return task;
    }

    @Override
    public void persist(@NotNull final Task entity) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "INSERT INTO " + tableName + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setDate(2, new Date(entity.getDateStart().getTime()));
        statement.setDate(3, new Date(entity.getDateFinish().getTime()));
        statement.setDate(4, new Date(entity.getDateCreated().getTime()));
        statement.setString(5, entity.getDescription());
        statement.setString(6, entity.getName());
        statement.setString(7, entity.getIdProject());
        statement.setString(8, entity.getOwnerId());
        statement.setString(9, entity.getStatus().toString());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final String entityId, @NotNull final Task entity) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "UPDATE " + tableName + " SET dateStart=?, dateFinish=?, dateCreate=?, " +
                "description=?, name=?, project_id=?, user_id=?, status=? WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setDate(1, new Date(entity.getDateStart().getTime()));
        statement.setDate(2, new Date(entity.getDateFinish().getTime()));
        statement.setDate(3, new Date(entity.getDateCreated().getTime()));
        statement.setString(4, entity.getDescription());
        statement.setString(5, entity.getName());
        statement.setString(6, entity.getIdProject());
        statement.setString(7, entity.getOwnerId());
        statement.setString(8, entity.getStatus().toString());
        statement.setString(9, entityId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName + " WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String ownerId, @NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName + " WHERE id=? AND user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        statement.setString(2, ownerId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAll() throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName;
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAll(@NotNull final String ownerId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName + " WHERE user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, ownerId);
        statement.executeUpdate();
        statement.close();

    }

    @Nullable
    private Task fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(ID_FIELD));
        task.setOwnerId(row.getString(OWNER_ID_FIELD));
        task.setDescription(row.getString(DESCRIPTION_FIELD));
        task.setDateCreated(row.getDate(DATE_CREATE_FIELD));
        task.setDateStart(row.getDate(DATE_START_FIELD));
        task.setDateFinish(row.getDate(DATE_FINISH_FIELD));
        task.setName(row.getString(NAME_FIELD));
        task.setIdProject(row.getString(PROJECT_ID_FIELD));
        @Nullable final String status = row.getString(STATUS_FIELD).toUpperCase();
        @Nullable ExecutionStatus executionStatus;
        try {
            executionStatus = ExecutionStatus.valueOf(status);
        } catch (IllegalArgumentException e) {
            executionStatus = ExecutionStatus.PLANNED;
        }
        task.setStatus(executionStatus);
        return task;
    }
}
