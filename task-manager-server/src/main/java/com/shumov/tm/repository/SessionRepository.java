package com.shumov.tm.repository;

import com.shumov.tm.api.repository.ISessionRepository;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.entity.Session;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    private final int ID_FIELD = 1;
    private final int SIGNATURE_FIELD = 2;
    private final int TIMESTAMP_FIELD = 3;
    private final int USER_ID_FIELD = 4;
    private final int OWNER_ID_FIELD = 5;

    private final String tableName = "app_session";

    @Override
    public @NotNull List<Session> findAll() throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final List<Session> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + tableName;
        @NotNull final Statement statement = connection.createStatement();
        @NotNull ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }


    @Override
    @Nullable
    public Session findOne(@NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Session session = fetch(resultSet);
        statement.close();
        if (session == null) return null;
        else return session;
    }

    @Override
    public void persist(@NotNull final Session entity) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "INSERT INTO " + tableName + " VALUES (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getSignature());
        statement.setLong(3, entity.getTimestamp());
        statement.setString(4, entity.getUserId());
        statement.setString(5, entity.getOwnerId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final String entityId, @NotNull final Session entity) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "UPDATE " + tableName + " SET signature=?, timestamp=?, " +
                "user_id=?, service_id=? WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getSignature());
        statement.setLong(2, entity.getTimestamp());
        statement.setString(3, entity.getUserId());
        statement.setString(4, entity.getOwnerId());
        statement.setString(8, entityId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName + " WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAll() throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName;
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @Nullable
    private Session fetch ( @Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(ID_FIELD));
        session.setSignature(row.getString(SIGNATURE_FIELD));
        session.setTimestamp(row.getLong(TIMESTAMP_FIELD));
        session.setUserId(row.getString(USER_ID_FIELD));
        session.setOwnerId(row.getString(OWNER_ID_FIELD));
        return session;
    }
}
