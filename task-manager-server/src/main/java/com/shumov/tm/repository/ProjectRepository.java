package com.shumov.tm.repository;


import com.shumov.tm.api.repository.IProjectRepository;
import com.shumov.tm.api.service.ServiceLocator;
import com.shumov.tm.entity.Project;
import com.shumov.tm.enumerate.ExecutionStatus;
import com.shumov.tm.exception.service.ServiceLocatorNotInitialized;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    private final int ID_FIELD = 1;
    private final int DATE_START_FIELD = 2;
    private final int DATE_FINISH_FIELD = 3;
    private final int DATE_CREATE_FIELD = 4;
    private final int DESCRIPTION_FIELD = 5;
    private final int NAME_FIELD = 6;
    private final int OWNER_ID_FIELD = 7;
    private final int STATUS_FIELD = 8;

    private final String tableName = "app_project";

    @Override
    @NotNull
    public List<Project> findAll() throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final String query = "SELECT * FROM " + tableName;
        @NotNull final Statement statement = connection.createStatement();
        @NotNull ResultSet resultSet = statement.executeQuery(query);
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final String ownerId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE user_id=?";
        @NotNull final List<Project> result = new ArrayList<>();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, ownerId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        statement.close();
        if (project == null) return null;
        else return project;
    }

    @Override
    @Nullable
    public Project findOne(
            @NotNull final String ownerId,
            @NotNull final String entityId
    ) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "SELECT * FROM " + tableName + " WHERE id=? AND user_id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        statement.setString(2, ownerId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        statement.close();
        if (project == null) return null;
        else return project;
    }

    @Override
    public void persist(@NotNull final Project entity) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "INSERT INTO " + tableName + " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setDate(2, new Date(entity.getDateStart().getTime()));
        statement.setDate(3, new Date(entity.getDateFinish().getTime()));
        statement.setDate(4, new Date(entity.getDateCreated().getTime()));
        statement.setString(5, entity.getDescription());
        statement.setString(6, entity.getName());
        statement.setString(7, entity.getOwnerId());
        statement.setString(8, entity.getStatus().toString());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final String entityId, @NotNull final Project entity) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "UPDATE " + tableName + " SET dateStart=?, dateFinish=?, dateCreate=?, " +
                "description=?, name=?, user_id=?, status=? WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setDate(1, new Date(entity.getDateStart().getTime()));
        statement.setDate(2, new Date(entity.getDateFinish().getTime()));
        statement.setDate(3, new Date(entity.getDateCreated().getTime()));
        statement.setString(4, entity.getDescription());
        statement.setString(5, entity.getName());
        statement.setString(6, entity.getOwnerId());
        statement.setString(7, entity.getStatus().toString());
        statement.setString(8, entityId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String entityId) throws Exception {
        if (connection == null) throw new ServiceLocatorNotInitialized();
        @NotNull final String query = "DELETE FROM " + tableName + " WHERE id=?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entityId);
        statement.executeUpdate();
        statement.close();
    }

        @Override
        public void remove(@NotNull final String ownerId, @NotNull final String entityId) throws Exception {
            if (connection == null) throw new ServiceLocatorNotInitialized();
            @NotNull final String query = "DELETE FROM " + tableName + " WHERE id=? AND user_id=?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, entityId);
            statement.setString(2, ownerId);
            statement.executeUpdate();
            statement.close();
        }

        @Override
        public void removeAll() throws Exception {
            if (connection == null) throw new ServiceLocatorNotInitialized();
            @NotNull final String query = "DELETE FROM " + tableName;
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
            statement.close();
        }

        @Override
        public void removeAll (@NotNull final String ownerId) throws Exception {
            if (connection == null) throw new ServiceLocatorNotInitialized();
            @NotNull final String query = "DELETE FROM " + tableName + " WHERE user_id=?";
            @NotNull final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, ownerId);
            statement.executeUpdate();
            statement.close();
        }

        @Nullable
        private Project fetch ( @Nullable final ResultSet row) throws SQLException {
            if (row == null) return null;
            @NotNull final Project project = new Project();
            project.setId(row.getString(ID_FIELD));
            project.setOwnerId(row.getString(OWNER_ID_FIELD));
            project.setDescription(row.getString(DESCRIPTION_FIELD));
            project.setDateCreated(row.getDate(DATE_CREATE_FIELD));
            project.setDateStart(row.getDate(DATE_START_FIELD));
            project.setDateFinish(row.getDate(DATE_FINISH_FIELD));
            project.setName(row.getString(NAME_FIELD));
            @Nullable final String status = row.getString(STATUS_FIELD).toUpperCase();
            @Nullable ExecutionStatus executionStatus;
            try {
                executionStatus = ExecutionStatus.valueOf(status);
            } catch (IllegalArgumentException e) {
                executionStatus = ExecutionStatus.PLANNED;
            }
            project.setStatus(executionStatus);
            return project;
        }
    }

