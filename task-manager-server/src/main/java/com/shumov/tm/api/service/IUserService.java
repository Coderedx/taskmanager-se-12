package com.shumov.tm.api.service;

import com.shumov.tm.entity.User;
import com.shumov.tm.entity.dto.UserDTO;
import com.shumov.tm.enumerate.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.List;


public interface IUserService extends Service {

    @Nullable
    User getUserByLogin(@Nullable final String login,
                        @Nullable final String password) throws Exception;

    void mergeUser(@Nullable final User user) throws Exception;

    void createNewUser(@Nullable final String login,
                       @Nullable final String password) throws Exception;

    void createNewUser(@Nullable final String login,
                       @Nullable final String password,
                       @NotNull final Role userRole) throws Exception;

    void isCorrectLogin(@Nullable final String login) throws IOException;

    void isCorrectPass(@Nullable final String pass) throws IOException;

    void isCorrectDescription(@Nullable final String description) throws IOException;

    @NotNull
    List<User> getUserList() throws Exception;

    void setUsers(@NotNull final List<User> users) throws Exception;

    @NotNull
    User getUser(@Nullable final String id) throws Exception;

    void editDescription(@NotNull final String userId,
                         @NotNull final String description) throws Exception;

    void editPassword(@NotNull final String userId,
                                   @NotNull final String password) throws Exception;

    UserDTO reviewUser(@NotNull final String userId) throws Exception;

    int checkCount() throws Exception;
}
