package com.shumov.tm.api.service;

import com.shumov.tm.api.entity.Entity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.Date;

public interface Service {

    void isCorrectInputData(@Nullable final String data) throws Exception;

    void isCorrectObject(@Nullable final Entity entity) throws Exception;

    @NotNull Date parseDate(@Nullable final String date) throws IOException;
}
