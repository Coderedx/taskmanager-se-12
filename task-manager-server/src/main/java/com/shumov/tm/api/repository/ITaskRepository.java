package com.shumov.tm.api.repository;

import com.shumov.tm.entity.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull
    public List<Task> findAll(@NotNull final String ownerId) throws Exception;

    @Nullable
    public Task findOne(
            @NotNull final String ownerId,
            @NotNull final String entityId
    ) throws Exception;

    public void remove(@NotNull final String ownerId, @NotNull final String entityId) throws Exception;

    public void removeAll(@NotNull final String ownerId) throws Exception;
}
