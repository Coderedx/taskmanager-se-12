package com.shumov.tm.api.service;

import java.sql.Connection;

public interface ServiceLocator {
    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    IDomainService getDomainService();

    ISessionService getSessionService();

    Connection getConnectionDB();
}
