package com.shumov.tm.api.service;

import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Task;
import com.shumov.tm.enumerate.ExecutionStatus;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;


public interface IProjectService extends Service {

    void createProject(@Nullable final String ownerId, @Nullable final String name) throws Exception;

    @NotNull
    List<Project> getProjectList() throws Exception;

    @NotNull
    List<Project> getProjectList(@Nullable final String ownerId) throws Exception;

    void setProjects(@NotNull final List<Project> projects) throws Exception;

    @NotNull
    Project getProject(@Nullable final String id) throws Exception;

    @NotNull
    Project getProject(@Nullable final String ownerId, @Nullable final String id) throws Exception;

    void editProjectNameById(@Nullable final String id, @Nullable final String name) throws Exception;

    void editProjectNameById(@Nullable final String ownerId,
                             @Nullable final String id,
                             @Nullable final String name) throws Exception;

    void editProjectDate(@Nullable final String ownerId,
                         @Nullable final String id,
                         @Nullable final String start,
                         @Nullable final String finish) throws Exception;

    void editProjectStatus(@Nullable String ownerId,
                           @Nullable String id,
                           @NotNull String status) throws Exception;

    void removeProjectById(@Nullable final String id) throws Exception;

    void removeProjectById(@Nullable final String ownerId,
                           @Nullable final String id) throws Exception;

    void clearProjects() throws Exception;

    void clearProjects(@NotNull final String ownerId) throws Exception;

    @NotNull
    List<Project> searchProjects(@NotNull final String ownerId, @NotNull final String term) throws Exception;

    @NotNull
    List<Project> getProjectSortedList(@Nullable final String ownerId,
                                              @Nullable final String method) throws Exception;
    @NotNull
    List<Task> getProjectTasksById(@Nullable final String ownerId,
                                          @NotNull final String projectId) throws Exception;
}
