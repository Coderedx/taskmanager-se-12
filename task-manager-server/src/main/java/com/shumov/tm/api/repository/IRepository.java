package com.shumov.tm.api.repository;

import com.shumov.tm.api.entity.Entity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.List;

public interface IRepository<T extends Entity> {

    @NotNull
    List<T> findAll() throws Exception;

    @Nullable
    T findOne(@NotNull final String entityId) throws Exception;

    void persist(@NotNull final T entity) throws Exception;

    void merge(@NotNull final String entityId, @NotNull final T entity) throws Exception;

    void remove(@NotNull final String entityId) throws Exception;

    void removeAll() throws Exception;

    void setConnection(@Nullable Connection connection);
}
