package com.shumov.tm.api.service;

import com.shumov.tm.entity.Project;
import com.shumov.tm.entity.Session;
import com.shumov.tm.entity.Task;
import com.shumov.tm.entity.dto.UserDTO;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IAdminService {

    void projectClearDB() throws Exception;

    void projectEditName(@NotNull String projectId, @NotNull String name) throws Exception;

    @NotNull List<Project> getProjectList() throws Exception;

    void removeProjectById(@NotNull String projectId) throws Exception;

    @NotNull List<Task> getProjectTasksById(@NotNull String projectId) throws Exception;

    void addTaskInProject(@NotNull String projectId, @NotNull String taskId) throws Exception;

    void taskClearDB() throws Exception;

    void taskEditName(@NotNull String taskId, @NotNull String name) throws Exception;

    @NotNull List<Task> getTaskList() throws Exception;

    void removeTaskById(@NotNull String taskId) throws Exception;

    void regAdmin(@NotNull String login, @NotNull String pass) throws Exception;

    @NotNull UserDTO reviewUser(@NotNull Session session) throws Exception;

    void dataLoadBin() throws Exception;

    void dataSaveBin() throws Exception;

    void dataLoadFasterJSON() throws Exception;

    void dataLoadFasterXML() throws Exception;

    void dataSaveFasterJSON() throws Exception;

    void dataSaveFasterXML() throws Exception;

    void dataLoadJaxBJSON() throws Exception;

    void dataLoadJaxBXML() throws Exception;

    void dataSaveJaxBJSON() throws Exception;

    void dataSaveJaxBXML() throws Exception;
}
