package com.shumov.tm.api.repository;

import com.shumov.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    List<User> findAll() throws Exception;

    @Override
    @Nullable
    User findOne(@NotNull final String id) throws Exception;

    @Nullable
    User findOneByLogin(@NotNull final String login) throws Exception;

    @Override
    void persist(@NotNull final User user) throws Exception;

    void merge(@NotNull final String entityId, @NotNull final User entity) throws Exception;

    void remove(@NotNull final String entityId) throws Exception;

    void removeAll() throws Exception;

    int checkCount() throws Exception;
}
