package com.shumov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.util.Properties;

public class ConnectorDBUtil {

    public static void registerDriverJDBC(){
        try {
            @NotNull final Driver driver = new com.mysql.jdbc.Driver();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection connectionDB() {
        try{
        registerDriverJDBC();
        @Nullable Connection connection;
        @NotNull final File workingFolder = new File("task-manager-server"+File.separator+"src"+File.separator+
                "main"+File.separator+"resources");
        @NotNull final File properties = new File(workingFolder, "ConnectionDB.properties");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(properties);
        @NotNull final Properties property = new Properties();
        property.load(fileInputStream);
        @NotNull final String login = property.getProperty("db.login");
        @NotNull final String password = property.getProperty("db.password");
        @NotNull final String host = property.getProperty("db.host");
        connection = DriverManager.getConnection(host, login,password);
        return connection;
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
