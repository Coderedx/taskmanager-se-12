package com.shumov.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.Nullable;

public class SignatureUtil {

    @Nullable
    public static String sign(final Object value, String salt, Integer cycle){
        try{
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (final JsonProcessingException e){
            return null;
        }
    }

    @Nullable
    public static String sign(
            @Nullable final String value,
            @Nullable final String salt,
            @Nullable final Integer cycle)
    {
        if(value==null || salt == null || cycle == null) return null;
        String result = value;
        for(int i = 0; i < cycle; i++){
            result = HashUtil.getMd5(salt+value+salt);
        }
        return result;
    }


}
