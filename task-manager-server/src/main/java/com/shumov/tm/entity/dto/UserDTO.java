package com.shumov.tm.entity.dto;

import com.shumov.tm.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public class UserDTO {

    @Nullable
    private String login;
    @Nullable
    private Role role;
    @Nullable
    private String description;

}
