package com.shumov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.enumerate.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends AbstractEntity implements Serializable {

    @Nullable
    private String login;
    @Nullable
    private String password;
    @NotNull
    private Role role = Role.GUEST;
    @Nullable
    private String description;

    public User(@NotNull final String login, @NotNull final String password)
    {
        this.login = login;
        this.password = password;
        this.role = Role.USER;
    }

    public User(
        @NotNull final String login,
        @NotNull final String password,
        @NotNull Role role)
    {
        this.login = login;
        this.password = password;
        this.role = role;
    }
}
