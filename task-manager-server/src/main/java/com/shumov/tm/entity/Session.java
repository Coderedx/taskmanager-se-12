package com.shumov.tm.entity;


import com.shumov.tm.api.entity.Entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public final class Session implements Cloneable, Entity {

    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
    @NotNull
    private String id = UUID.randomUUID().toString();
    @NotNull
    private Long timestamp = 1000L;
    @Nullable
    private String userId;
    @Nullable
    private String signature;
    @Nullable
    private String ownerId;

}
