package com.shumov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.shumov.tm.api.entity.Entity;
import com.shumov.tm.enumerate.ExecutionStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends AbstractEntity implements Entity, Serializable {

    @NotNull
    private String name = "";
    @NotNull
    private String description = "";
    @NotNull
    private String idProject = "";
    @NotNull
    private Date dateStart = new Date();
    @NotNull
    private Date dateFinish = new Date();
    @NotNull
    private ExecutionStatus status = ExecutionStatus.PLANNED;


    public Task(
        @NotNull final String ownerId,
        @NotNull final String name,
        @NotNull final String idProject)
    {
        this.name = name;
        this.idProject = idProject;
        this.ownerId = ownerId;
    }
}
